#!/bin/bash

BASE_DIR=/home/ec2-user/KafkaApps
APP_NAME=$2
CONFIG_FILE=$BASE_DIR/Monitor/config/monitor_script.cfg

PID_FILE=$BASE_DIR/${APP_NAME}/${APP_NAME}.pid
LOG_DIR=$BASE_DIR/${APP_NAME}/logs

get_jar_file() {
  cd $BASE_DIR/${APP_NAME}
  jar_file_name=`ls -t *-spring*boot*.jar | head -1`
  echo $jar_file_name
}

get_env() {
  ENV=`cat $CONFIG_FILE | grep ENV | awk -F= '{print $2}'`
  echo $ENV
}

start() {
  jar_file_name=`get_jar_file`
  env=`get_env`
  if [ "$APP_NAME" == "kafkaStreams" ]; then
        START_COMMAND="sudo java -Xmx512m -Xms256m -jar $BASE_DIR/${APP_NAME}/${jar_file_name} ${env}"
  else
        START_COMMAND="sudo java -jar $BASE_DIR/${APP_NAME}/${jar_file_name} ${env}"
  fi
  cd $BASE_DIR/${APP_NAME}
  PID=`$START_COMMAND > $BASE_DIR/${APP_NAME}/console.${APP_NAME}.log 2>$BASE_DIR/${APP_NAME}/console.${APP_NAME}.error.log & echo $!`
}

case "$1" in
start)
    if [ -f $PID_FILE ]; then
        PID=`cat $PID_FILE`
        ps axf | grep ${PID} | grep -v grep > /dev/null
        if [ $? -ne 0 ]; then
            start
        else
            echo "Running [$PID]"
            exit 0
        fi
    else
        start
    fi

    if [ -z $PID ]; then
        echo "Failed starting"
        exit 1
    else
        echo $PID > $PID_FILE
        echo "Running [$PID]"
        exit 0
    fi
;;
status)
    if [ -f $PID_FILE ]; then
        PID=`cat $PID_FILE`
        ps axf | grep ${PID} | grep -v grep > /dev/null
        if [ $? -ne 0 ]; then
            echo "Stopped"
            exit 1
        else
            echo "Running"
            exit 0
        fi
    else
        echo "Stopped"
        exit 0
    fi
;;
stop)
    if [ -f $PID_FILE ]; then
        PID=`cat $PID_FILE`
        ps axf | grep ${PID} | grep -v grep > /dev/null
        if [ $? -ne 0 ]; then
            echo "Not running (process dead but PID file exists)"
            rm -f $PID_FILE
            exit 1
        else
            PID=`cat $PID_FILE`
            sudo kill $PID
            echo "Stopped [$PID]"
            rm -f $PID_FILE
            exit 0
        fi
    else
        echo "Not running (PID not found)"
        exit 0
    fi
;;
restart)
    $0 stop $APP_NAME
    $0 start $APP_NAME
;;
*)
    echo "Usage: $0 {status|start|stop|restart} {appName}"
    echo "Example Usage: ./kafka-service.sh start kafkaStreams"
    exit 0
esac
