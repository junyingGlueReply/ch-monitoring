#!/bin/bash

USER_HOME=/home/ec2-user
STREAMING_HOME=${USER_HOME}/KafkaApps
MONITOR_HOME=${STREAMING_HOME}/Monitor
SERVICE_SCRIPT=${MONITOR_HOME}/bin/kafka-service.sh
RESTART_SCRIPT=${MONITOR_HOME}/bin/service-restart.sh
CONFIG_FILE=${MONITOR_HOME}/config/monitor_script.cfg

while true
do
   for APP_NAME in `cat $CONFIG_FILE | grep APP_LIST | awk -F= '{print $2}'`
   do
     status=`$SERVICE_SCRIPT status $APP_NAME`
     statusArray=($status)
     if [ "${statusArray[0]}" != "Running" ]; then
        echo "$APP_NAME service is not running" | xargs -L 1 echo $(date +'[%Y-%m-%d %H:%M:%S]') $1 >> ${STREAMING_HOME}/${APP_NAME}/logs/${APP_NAME}-pid-logger.log
        $RESTART_SCRIPT ${APP_NAME} > /dev/null
     fi
   done
   sleep 10m
done